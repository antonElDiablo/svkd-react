<?php
/**
 * This view is used by console/controllers/MigrateController.php
 * The following variables are available in this view:
 */
/* @var $className string the new migration class name */

echo "<?php\n";
?>

use yii\db\Schema;
use yii\db\Migration;

class <?= $className ?> extends Migration
{
    /*
    public $table_for_add_columns = ["mm_company"];
    public $add_column_names = ["name2"=>["string(200)"],];
    */
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        /*$this->createTable('{{%tablename}}', [
            'id' =>             $this->primaryKey(),
            'title' =>          $this->string(500)." COMMENT 'Comment'",
        ], $tableOptions);*/
        //$this->addForeignKey    ('FK_NAME', '{{%tablename}}', 'reestr_id', '{{%mm_reestr}}', 'id', 'cascade', 'cascade');
        if (isset($this->add_column_names) && is_array($this->add_column_names))
        {
            foreach ($this->add_column_names as $key=>$name) {
                $add = null;
                foreach ($name as $nam) {
                    $add .= $nam;
                }
                foreach ($this->table_for_add_columns as $k => $table) {
                    $this->addColumn($table, $key, $add);
                }
            }
        }
    }

    public function down()
    {
        if (isset($this->add_column_names) && is_array($this->add_column_names))
        {
            foreach ($this->add_column_names as $key=>$name) {
                foreach ($this->table_for_add_columns as $k => $table) {
                    $this->dropColumn($table, $key);
                }
            }
        }
        //$this->dropForeignKey   ('foreignkey', '{{%tablename}}');
        //$this->dropTable        ('{{%tablename}}');
    }
    
    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }
    
    public function safeDown()
    {
    }
    */
}
