<?php
/**
 * Yii2 Shortcuts
 * @author Eugene Terentev <eugene@terentev.net>
 * -----
 * This file is just an example and a place where you can add your own shortcuts,
 * it doesn't pretend to be a full list of available possibilities
 * -----
 */

/**
 * @return int|string
 */
function getMyId()
{
    return Yii::$app->user->getId();
}

/**
 * @param string $view
 * @param array $params
 * @return string
 */
function render($view, $params = [])
{
    return Yii::$app->controller->render($view, $params);
}

/**
 * @param $url
 * @param int $statusCode
 * @return \yii\web\Response
 */
function redirect($url, $statusCode = 302)
{
    return Yii::$app->controller->redirect($url, $statusCode);
}

/**
 * @param $form \yii\widgets\ActiveForm
 * @param $model
 * @param $attribute
 * @param array $inputOptions
 * @param array $fieldOptions
 * @return string
 */
function activeTextinput($form, $model, $attribute, $inputOptions = [], $fieldOptions = [])
{
    return $form->field($model, $attribute, $fieldOptions)->textInput($inputOptions);
}

/**
 * @param string $key
 * @param mixed $default
 * @return mixed
 */
function env($key, $default = false) {

    $value = getenv($key);

    if ($value === false) {
        return $default;
    }

    switch (strtolower($value)) {
        case 'true':
        case '(true)':
            return true;

        case 'false':
        case '(false)':
            return false;
    }

    return $value;
}

/**
 * @param mixed $var
 * @param bool $showHtml
 * @param bool $showFrom
 * @return bool
 */
function d($var = false, $showHtml = false, $showFrom = true) {
    if (!YII_DEBUG) return false;
    echo '<div class="debug">';
    if ($showFrom) {
        $calledFrom = debug_backtrace();
        echo '<strong>' . $calledFrom[0]['file'] . '</strong>';
        echo ' (line <strong>' . $calledFrom[0]['line'] . '</strong>)';
    }
    echo "\n<pre class=\"debug\">\n";

    $var = print_r($var, true);
    if ($showHtml) {
        $var = str_replace('<', '&lt;', str_replace('>', '&gt;', $var));
    }
    echo $var . "\n</pre></div>\n\n";
}

/**
 * @param array $arra array of photo data
 * @return string
 */
function get_src($arra) {
    if (is_array($arra)) {
        return Yii::getAlias("@storageUrl") . '/source' . '/' . $arra['path'];
    }
}

/**
 * @param string $str
 * @param string $length
 * @param string $postfix
 * @param string $encoding
 * @return string
 */
function mbCutString($str, $length, $postfix = '...', $encoding = 'UTF-8') {
    if (mb_strlen($str, $encoding) <= $length) {
        return strip_tags($str);
    }

    $tmp = mb_substr($str, 0, $length, $encoding);
    return strip_tags(mb_substr($tmp, 0, mb_strripos($tmp, ' ', 0, $encoding), $encoding) . $postfix);
}